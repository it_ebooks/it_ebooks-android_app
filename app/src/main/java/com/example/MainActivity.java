// Utilise avec DBHelper
package com.example;

import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button b;
    ListView lv;
    DBHelper dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b = (Button) findViewById(R.id.button);
        lv = (ListView) findViewById(R.id.listView);

        b.setOnClickListener(this);

        dbh = new DBHelper(this);
        Cursor c = dbh.listeLivres();
        Log.d("DBH", "nombre de livres = " + c.getCount());

        c.moveToFirst();
        while(!c.isAfterLast()) {
            Log.d("DBH", "id = " + c.getInt(c.getColumnIndex(DBHelper.B_ID)) +
                    " titre = " + c.getString(c.getColumnIndex(DBHelper.B_TITLE)));
            c.moveToNext();
        }

        // Afficher dans la ListeView
        String[] from = {DBHelper.B_ID, DBHelper.B_TITLE};
        int[] to = {0, android.R.id.text1};
        SimpleCursorAdapter sca = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1,
                c, from, to, 0);
        lv.setAdapter(sca);
    }

    @Override
    public void onClick(View v) {
        DownloadTask dt = new DownloadTask();
        dt.execute();
    }

    public class DownloadTask extends AsyncTask<Void, Void, Void> {
        int nb = -1;

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(MainActivity.this, "nouveaux livres = " + nb, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            WebAPI web = new WebAPI(MainActivity.this);
            try {
                nb = web.update();
            } catch (Exception e) {};

            return null;
        }
    }
}
