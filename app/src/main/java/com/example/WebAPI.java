package com.example;

import android.content.Context;
import android.util.Log;

import com.example.json.Book;
import com.example.json.Root;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class WebAPI {
    public String url;
    DBHelper dbh;

    // public WebAPI(Context context){
    public WebAPI(String id){
        url = "http://it-ebooks-api.info/v1/book/" + id; // url du json
    }

    public WebAPI(Context context){
        url = "http://it-ebooks-api.info/v1/search/php"; // url du json
        dbh = new DBHelper(context);
    }

    public WebAPI(String id, int n) {
        url= "http://it-ebooks-api.info/v1/search/" + id;
    }

    public Book getBook() throws IOException{
        // recuperer le contenu html a partir d'une url
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        String json = response.body().string();

        // parser le contenu json a partir de la String json
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<Book> jsonAdapter = moshi.adapter(Book.class);
        Book book = jsonAdapter.fromJson(json);

        return book;
    }

    public Root getSearch() throws  IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        String json = response.body().string();

        // parser le contenu json a partir de la String json
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<Root> jsonAdapter = moshi.adapter(Root.class);
        Root root = jsonAdapter.fromJson(json);

        return root;
    }

    public int update() throws  IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        String json = response.body().string();

        // parser le contenu json a partir de la String json
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<Root> jsonAdapter = moshi.adapter(Root.class);
        Root root = jsonAdapter.fromJson(json);

        // recuperer les livres
        int nb = 0;
        nb = dbh.ajouterLivres(root);

        return nb;
    }
}
