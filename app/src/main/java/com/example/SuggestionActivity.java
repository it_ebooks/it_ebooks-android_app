// Propose des suggestion des differrentes categories des livres a lire

package com.example;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.json.Book;
import com.example.json.Root;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.util.ArrayList;

public class SuggestionActivity extends AppCompatActivity {

    ArrayList<RecyclerView> lists;
    ArrayList<WebAPI> webs;
    ArrayList<Root> searchs;
    ArrayList<ArrayList<Book>> books;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher);

        lists = new ArrayList<>();
        searchs = new ArrayList<>();
        webs = new ArrayList<>();
        books = new ArrayList<>();

        Toast.makeText(getBaseContext(), "Creating suggestions...", Toast.LENGTH_LONG).show();

        new RunAPI().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.suggestions_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Choix des menus
        switch(item.getItemId()) {
            case R.id.search: startActivity(new Intent(this,SearchActivity.class)); break;
            case R.id.about: startActivity(new Intent(this,AboutActivity.class)); break;
            case R.id.library: startActivity(new Intent(this,BookshelfActivity.class)); break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyRecycleAdapter extends RecyclerView.Adapter<MyRecycleAdapter.MyViewHolder> {

        int type;

        public MyRecycleAdapter(int type) {
            this.type = type;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView image;
            public MyViewHolder(View itemView) {
                super(itemView);
                image = (ImageView) itemView.findViewById(R.id.image);
            }
        }

        @Override
        public MyRecycleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);

            View contactView = inflater.inflate(R.layout.suggestion_list_layout, parent, false);
            MyViewHolder viewHolder = new MyViewHolder(contactView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(MyRecycleAdapter.MyViewHolder holder, int position) {

            try {
                ImageView imageView = holder.image;
                String url = searchs.get(type).Books.get(position).Image;
                Picasso.with(getApplicationContext()).load(url).into(imageView);
                imageView.setOnClickListener(new BookListener(type, position));
            } catch (Exception e) {
                Toast.makeText(getBaseContext(), "Can't connect to the server. Check your internet connection.", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public int getItemCount() {
            int n = 10; // nombre des livres a afficher (optimal 7-10 livres)
            return n;
        }
    }

    private class RunAPI extends AsyncTask<Object,Object,Root> {

        // Suggestions
        @Override
        protected Root doInBackground(Object... params) {
            webs.add(0, new WebAPI("linux", 0));
            webs.add(1, new WebAPI("java", 0));
            webs.add(2, new WebAPI("android", 0));
            webs.add(3, new WebAPI("python", 0));
            webs.add(4, new WebAPI("oracle", 0));

            try {
                for(WebAPI web : webs)
                    searchs.add(web.getSearch());

                for(int i=0; i < searchs.size();i++) {
                    books.add(i, new ArrayList<Book>());

                    for (int j = 0; j < searchs.get(i).Books.size(); j++)
                        books.get(i).add(j, searchs.get(i).Books.get(j));
                }
            }
            catch(IOException e) {}

            return null;
        }

        @Override
        protected void onPostExecute(Root search) {
            super.onPostExecute(search);
                lists.add((RecyclerView) findViewById(R.id.list0));
                lists.add((RecyclerView) findViewById(R.id.list1));
                lists.add((RecyclerView) findViewById(R.id.list11));
                lists.add((RecyclerView) findViewById(R.id.list12));
                lists.add((RecyclerView) findViewById(R.id.list13));

            for(int i=0;i<lists.size(); i++) {
                lists.get(i).setAdapter(new MyRecycleAdapter(i));
                LinearLayoutManager layoutManager = new LinearLayoutManager(SuggestionActivity.this, LinearLayoutManager.HORIZONTAL, false);
                lists.get(i).setLayoutManager(layoutManager);
            }
        }
    }

    class BookListener implements View.OnClickListener {

        int type;
        int position;

        public BookListener(int type, int position) {
            this.type = type;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            String bookID = books.get(type).get(position).ID;
            Intent intent = new Intent(SuggestionActivity.this,DescriptionActivity.class);
            intent.putExtra("BOOK", bookID);
            startActivity(intent);
        }
    }
}