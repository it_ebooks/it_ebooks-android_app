// Description detaillee d'un livre

package com.example;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.json.Book;
import com.squareup.picasso.Picasso;

public class DescriptionActivity extends AppCompatActivity implements View.OnClickListener {
    Book book;
    TextView title;
    TextView description;
    TextView author;
    TextView isbn;
    TextView year;
    TextView publisher;
    TextView page;
    TextView subtitle;
    ImageView image;
    CheckBox favorites;
    Button download;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        // Le logotype dans le menu
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher);

        download = (Button) findViewById(R.id.download);
        download.setOnClickListener(this);

        Intent intent = getIntent();
        String code = intent.getStringExtra("BOOK");

        title = (TextView) findViewById(R.id.title);
        subtitle = (TextView) findViewById(R.id.subtitle);
        description = (TextView) findViewById(R.id.description);
        author = (TextView) findViewById(R.id.author);
        isbn = (TextView) findViewById(R.id.isbn);
        year = (TextView) findViewById(R.id.year);
        publisher = (TextView) findViewById(R.id.publisher);
        page = (TextView) findViewById(R.id.page);
        favorites = (CheckBox) findViewById(R.id.btnAddToFavourite1);
        image = (ImageView) findViewById(R.id.image);
        image.setOnClickListener(this);
        RunAPI run = new RunAPI(code);
        run.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.description_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Choix des elements du menu
        switch(item.getItemId()) {
            case R.id.library: startActivity(new Intent(this,BookshelfActivity.class)); break;
            case R.id.search: startActivity(new Intent(this,SearchActivity.class)); break;
            case R.id.suggestions: startActivity(new Intent(this,SuggestionActivity.class)); break;
            case R.id.share: startActivity(new Intent(this,ShareActivity.class)); break;
            case R.id.about: startActivity(new Intent(this,AboutActivity.class)); break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        Intent intent;

        intent = new Intent(DescriptionActivity.this,DownloadActivity.class);
        intent.putExtra("TITLE", book.Title);
        intent.putExtra("URL", book.Download);
        intent.putExtra("ID", book.ID);
        startActivity(intent);
    }


    public class RunAPI extends AsyncTask<String, Object, Book> {

        private final String id;

        public RunAPI(String id) {
            this.id = id;
        }

        @Override
        protected Book doInBackground(String... params) {
            WebAPI webBook = new WebAPI(id);
            try {
                book = webBook.getBook();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Book root) {
            super.onPostExecute(root);

            try {
                title.setText(book.Title);
                subtitle.setText(book.SubTitle);
                description.setText(book.Description);
                author.setText(book.Author);
                isbn.setText(book.ISBN);
                year.setText(book.Year);
                publisher.setText(book.Publisher);
                page.setText(book.Page);
                String url = book.Image;
                Picasso.with(getApplicationContext()).load(url).into(image);
            } catch (Exception e) {
                Toast.makeText(getBaseContext(), "Can't connect to the server. Check your internet connection.", Toast.LENGTH_LONG).show();
            }

        }
    }
}