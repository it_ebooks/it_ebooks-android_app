// La base de donnees pour la liste des livres

package com.example;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.example.json.Book;
import com.example.json.Root;

public class DBHelper extends SQLiteOpenHelper {

    static final String DB_NAME = "books.db"; // nom du fichier de la base de donnees
    static final int DB_VERSION = 18; // la version du db

    // Des constantes specifiques pour la db (tableau du db)
    static final String TABLE_BOOKS = "books"; // Nom du tableau
    static final String B_ID = "_id"; // I-re colonne ("_id" obligatoire)
    static final String B_TITLE = "title"; // II colonne
    static final String B_DESCRIPTION = "description"; // III col.
    static final String B_FAVORITES = "favorites"; // IV col.

    private static SQLiteDatabase db = null; // on ouvre une seule db

    public DBHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
        if (db == null) db = getWritableDatabase(); // au debut la base de donnees est nule.
                                                    // Si elle est construite qqpart elle va etre ouverte pour toute le monde
    }

    // Cree le tableau
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table " + TABLE_BOOKS
                + " ( " + B_ID + " integer primary key, "  // champ obligatoir pour Android
                + B_TITLE + " text, "
                + B_DESCRIPTION + " text "
                + B_FAVORITES + " text )";
        Log.d("SQL onCreate", "sql = " + sql); // Log pour debugger
        db.execSQL(sql); // execution of database
    }

    // Efface et recree le tableau
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_BOOKS); // delete old table
        onCreate(db); // execute onCreate pour une nouvelle db
    }

    // Inserer l'element dans la table
    // Retourne lo nombre des elements ajoutees
    public int ajouterLivres(Root root) { // On a une liste de livres et on veut les inserer une a une dans la listview
        int nb = 0;
        Book livre; // un seul element de la liste des livres
        ContentValues cv = new ContentValues();
        for (int i = 0; i < root.Books.size(); i++) { // traverse toutes les livres. Cherche dans la liste le i-me livre
            livre = root.Books.get(i);
            cv.clear(); // vider
            cv.put(B_ID, i); // le i-me element
            cv.put(B_TITLE, livre.Title); // insere le titre
            cv.put(B_DESCRIPTION, livre.Description);  // insere le descriptif
            cv.put(B_FAVORITES, livre.Favorites);
            try {
                db.insertOrThrow(TABLE_BOOKS, null, cv); // ajoute les livres dans la db ?
                nb++; // compte le nb d'elements inseres avec succes
            } catch (SQLException e) {}
        }
        return nb;
    }

    // Requette (query) SQL. Recupere les elements de la table pour etre affiches
    public Cursor listeLivres(){
        Cursor c;
        c = db.rawQuery("select * from " + TABLE_BOOKS + " order by " + B_TITLE + " asc", null);
        Log.d("listeLivres", "liste = " + c.getCount());
        return c;
    }
}
