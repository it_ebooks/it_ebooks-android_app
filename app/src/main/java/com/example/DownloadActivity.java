package com.example;

import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class DownloadActivity extends AppCompatActivity {

    WebView web;
    String title;
    String URL;
    String ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher);

        Intent intent = getIntent();
        title = intent.getStringExtra("TITLE");
        URL = intent.getStringExtra("URL");
        ID = intent.getStringExtra("ID"); //
        web = (WebView) findViewById(R.id.webView);
        web.loadUrl(URL);
        web.getSettings().setJavaScriptEnabled(true);

        // Bloque les pop ups au moment du telechargement
        if (URL.contains("pop") || URL.contains("ads")) { // http://filepi.com/i/qZ4DCsR"
            web.getSettings().setBlockNetworkLoads(true);
        } else web.getSettings().setBlockNetworkLoads(false);

        WebSettings settings = web.getSettings();
        settings.setSupportMultipleWindows(true);

        // Nouveau client (browser)
        web.setWebViewClient(new WebViewClient());
        // fin bloquage pop ups.

        // WebView Download Manager. Permet de telecharger avec le WebView seulement sans utiliser le navigateur web
        web.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                Toast.makeText(getBaseContext(), "Downloading: " + title, Toast.LENGTH_LONG).show();
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/IT_eBooks/" + title + ".pdf"); // Repertoire destination
                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);
            }
        });
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.download_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Choix des elements du menu
        switch(item.getItemId()) {
            case R.id.library: startActivity(new Intent(this,BookshelfActivity.class)); break;
            case R.id.search: startActivity(new Intent(this,SearchActivity.class)); break;
            case R.id.suggestions: startActivity(new Intent(this,SuggestionActivity.class)); break;
            case R.id.about: startActivity(new Intent(this,AboutActivity.class)); break;
        }
        return super.onOptionsItemSelected(item);
    }
}